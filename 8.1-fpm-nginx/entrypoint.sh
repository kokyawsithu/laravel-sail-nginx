#!/bin/bash

printf "\n\nStarting PHP 8.1 daemon...\n\n"
#php-fpm --daemonize
service php8.1-fpm start --daemonize

printf "Starting Nginx...\n\n"
set -e

if [[ "$1" == -* ]]; then
    set -- nginx -g daemon off; "$@"
fi

exec "$@"
